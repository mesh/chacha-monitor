#!/usr/bin/env python3
"""Script for Tkinter GUI CHaChA emulator."""
from socket import AF_INET, socket, SOCK_STREAM
import threading
import tkinter
import time
from tkinter import Checkbutton, DISABLED, NORMAL


def n2m( s ):
    """Translates node index to MAC address."""
    if( s == '01' ):
        return '04:f0:21:10:c3:23'
    elif( s == '02' ):
        return '04:f0:21:10:c3:20'
    elif( s == '03' ):
        return '04:f0:21:10:c8:0f'
    elif( s == '04' ):
        return '04:f0:21:10:c3:4f'
    elif( s == '05' ):
        return '04:f0:21:10:c2:90'
    elif( s == '06' ):
        return '04:f0:21:10:c2:8a'
    elif( s == '07' ):
        return '04:f0:21:10:c8:18'
    elif( s == '08' ):
        return '04:f0:21:10:c3:2b'
    elif( s == '09' ):
        return '04:f0:21:10:c8:15'
    elif( s == '10' ):
        return '04:f0:21:10:c3:57'
    elif( s == '11' ):
        return '04:f0:21:10:c3:41'
    elif( s == '12' ):
        return '04:f0:21:10:c3:71'
    elif( s == '13' ):
        return '04:f0:21:10:c3:2a'
    elif( s == '14' ):
        return '04:f0:21:10:c3:62'
    elif( s == '15' ):
        return '04:f0:21:10:c3:55'
    elif( s == '16' ):
        return '04:f0:21:10:c2:8e'
    elif( s == '17' ):
        return '04:f0:21:0f:77:54'
    elif( s == '18' ):
        return '04:f0:21:10:c3:39'
    elif( s == '19' ):
        return '04:f0:21:10:c3:33'
    elif( s == '20' ):
        return '04:f0:21:10:c5:af'
    elif( s == '21' ):
        return '04:f0:21:10:c3:3f'
    elif( s == '22' ):
        return '04:f0:21:10:c3:51'
    elif( s == '23' ):
        return '04:f0:21:10:c3:4c'
    elif( s == '24' ):
        return '04:f0:21:10:c2:88'
    elif( s == '25' ):
        return '04:f0:21:10:c8:0d'
    elif( s == '26' ):
        return '04:f0:21:10:c3:53'
    elif( s == '27' ):
        return '04:f0:21:10:c5:52'
    elif( s == '28' ):
        return '04:f0:21:10:c8:17'
    elif( s == '29' ):
        return '04:f0:21:10:c5:51'
    elif( s == '30' ):
        return '04:f0:21:10:c3:59'
    elif( s == '31' ):
        return '04:f0:21:10:c3:52'
    elif( s == '32' ):
        return '04:f0:21:10:c3:54'
    elif( s == '33' ):
        return '04:f0:21:10:c3:38'
    elif( s == '34' ):
        return '04:f0:21:10:c3:65'
    elif( s == '35' ):
        return '04:f0:21:10:c3:21'
    elif( s == '36' ):
        return '04:f0:21:10:c5:53'
    elif( s == '40' ):
        return '04:f0:21:14:c6:5e'


# Localhost
HOST = '127.0.0.1'
PORT = 39123

# PC Michael
#HOST = '139.30.201.114'
#PORT = 39123


# CHaChA testbed channels: 36 40 44 48 153 157
neighbour = ''
position_counter = 1
cycle_counter = 0

mesh2x2 = [ '|'+'CI'+'|'+n2m('07')+'|'+'36'+'|'+n2m('01')+'|'+n2m('02')+'|',
            '|'+'CI'+'|'+n2m('08')+'|'+'153'+'|']

mesh3x3 = [ '|'+'CI'+'|'+n2m('08')+'|'+'36'+'|'+n2m('01')+'|'+n2m('02')+'|'+n2m('03')+'|'+n2m('13')+'|'+n2m('14')+'|'+n2m('15')+'|',
            '|'+'CI'+'|'+n2m('07')+'|'+'153'+'|',
            '|'+'CI'+'|'+n2m('09')+'|'+'44'+'|']

mesh4x4 = [ '|'+'CI'+'|'+n2m('09')+'|'+'36'+'|'+n2m('01')+'|'+n2m('02')+'|'+n2m('03')+'|'+n2m('04')+'|'+n2m('08')+'|'+n2m('10')+'|'+n2m('15')+'|'+n2m('16')+'|'+n2m('22')+'|',
            '|'+'CI'+'|'+n2m('14')+'|'+'44'+'|'+n2m('07')+'|'+n2m('13')+'|'+n2m('19')+'|'+n2m('20')+'|'+n2m('21')+'|']

mesh5x5 = [ '|'+'CI'+'|'+n2m('15')+'|'+'36'+'|'+n2m('09')+'|'+n2m('14')+'|'+n2m('16')+'|'+n2m('21')+'|',
            '|'+'CI'+'|'+n2m('08')+'|'+'153'+'|'+n2m('01')+'|'+n2m('02')+'|'+n2m('07')+'|',
            '|'+'CI'+'|'+n2m('10')+'|'+'44'+'|'+n2m('03')+'|'+n2m('04')+'|'+n2m('05')+'|'+n2m('11')+'|'+n2m('17')+'|',
            '|'+'CI'+'|'+n2m('20')+'|'+'48'+'|'+n2m('13')+'|'+n2m('19')+'|'+n2m('25')+'|'+n2m('26')+'|'+n2m('27')+'|',
            '|'+'CI'+'|'+n2m('22')+'|'+'40'+'|'+n2m('23')+'|'+n2m('28')+'|'+n2m('29')+'|' ]

mesh6x6 = [ '|'+'CI'+'|'+n2m('08')+'|'+'36'+'|'+n2m('01')+'|'+n2m('02')+'|'+n2m('03')+'|'+n2m('07')+'|'+n2m('09')+'|'+n2m('13')+'|'+n2m('14')+'|'+n2m('15')+'|',
            '|'+'CI'+'|'+n2m('11')+'|'+'153'+'|'+n2m('04')+'|'+n2m('05')+'|'+n2m('06')+'|'+n2m('10')+'|'+n2m('12')+'|'+n2m('16')+'|'+n2m('17')+'|'+n2m('18')+'|',
            '|'+'CI'+'|'+n2m('26')+'|'+'44'+'|'+n2m('19')+'|'+n2m('20')+'|'+n2m('21')+'|'+n2m('25')+'|'+n2m('27')+'|'+n2m('31')+'|'+n2m('32')+'|'+n2m('33')+'|',
            '|'+'CI'+'|'+n2m('29')+'|'+'48'+'|'+n2m('22')+'|'+n2m('23')+'|'+n2m('24')+'|'+n2m('28')+'|'+n2m('30')+'|'+n2m('34')+'|'+n2m('35')+'|'+n2m('36')+'|']

meshRing = [ '|'+'CI'+'|'+n2m('26')+'|'+'36'+'|'+n2m('25')+'|'+n2m('27')+'|',
             '|'+'CI'+'|'+n2m('23')+'|'+'153'+'|'+n2m('17')+'|'+n2m('28')+'|'+n2m('29')+'|',
             '|'+'CI'+'|'+n2m('11')+'|'+'44'+'|'+n2m('04')+'|'+n2m('05')+'|',
             '|'+'CI'+'|'+n2m('02')+'|'+'48'+'|'+n2m('01')+'|'+n2m('03')+'|'+n2m('07')+'|',
             '|'+'CI'+'|'+n2m('19')+'|'+'40'+'|'+n2m('13')+'|']

meshL = [   '|'+'CI'+'|'+n2m('07')+'|'+'36'+'|'+n2m('01')+'|'+n2m('13')+'|',
            '|'+'CI'+'|'+n2m('19')+'|'+'153'+'|'+n2m('25')+'|',
            '|'+'CI'+'|'+n2m('02')+'|'+'44'+'|',
            '|'+'CI'+'|'+n2m('04')+'|'+'48'+'|'+n2m('03')+'|'+n2m('05')+'|']


meshLine = [    '|'+'CI'+'|'+n2m('03')+'|'+'36'+'|',
                '|'+'CI'+'|'+n2m('02')+'|'+'153'+'|'+n2m('01')+'|',
                '|'+'CI'+'|'+n2m('04')+'|'+'48'+'|'+n2m('05')+'|']

meshTriangle = [    '|'+'CI'+'|'+n2m('16')+'|'+'36'+'|'+n2m('09')+'|'+n2m('11')+'|'+n2m('15')+'|'+n2m('17')+'|'+n2m('23')+'|',
                    '|'+'CI'+'|'+n2m('10')+'|'+'44'+'|'+n2m('01')+'|'+n2m('02')+'|'+n2m('03')+'|'+n2m('04')+'|'+n2m('05')+'|'+n2m('08')+'|',
                    '|'+'CI'+'|'+n2m('22')+'|'+'48'+'|'+n2m('29')+'|']


meshPlus    = [     '|'+'CI'+'|'+n2m('15')+'|'+'36'+'|'+n2m('14')+'|'+n2m('16')+'|'+n2m('21')+'|'+n2m('27')+'|',
                    '|'+'CI'+'|'+n2m('09')+'|'+'44'+'|'+n2m('03')+'|'+n2m('13')+'|'+n2m('17')+'|']



gal40       =      '|'+'GAL40NH2CH'+'|'

# '|'++'|'++'|'++'|'++'|'++'|'++'|'++'|'++'|'++'|'++'|'++'|', blanc msg
# maybe we need additional rssi values for every connection between to nodes
# for mesh2x2rssi it might look like this:
# mesh2x2rssi = [   '|MCH07R120|CM01R120|CM02R120|',
#                   '|CH08R120|']

mesh5x5rssi = [ '|MCH15R120|CM09R120|CM14R120|CM16R120|CM21R120|',
                '|CH08R120|CM01R120|CM02R120|CM03R120|CM07R120|',
                '|CH10R120|CM04R120|CM05R120|CM11R120|',
                '|CH20R120|CM13R120|CM19R120|CM25R120|CM26R120|CM27R120|',
                '|CH22R120|CM17R120|CM23R120|CM28R120|CM29R120|' ]

mesh4x4rssi = [ '|MCH09R120|CM01R120|CM02R120|CM03R120|CM04R120|CM08R120|CM10R120|CM15R120|CM16R120|CM22R120|',
                '|CH14R120|CM07R120|CM13R120|CM19R120|CM20R120|CM21R120|']

mesh3x3rssi = [ '|MCH08R120|CM01R120|CM02R120|CM03R120|CM13R120|CM14R120|CM15R120|',
                '|CH07R120|',
                '|CH09R120|']

mesh2x2rssi = [ '|MCH07R120|CM01R120|CM02R120|',
                '|CH08R120|']

meshRingrssi = [ '|MCH26R120|CM25R120|CM27R120|',
                 '|CH23R120|CM17R120|CM28R120|CM29R120|',
                 '|CH11R120|CM04R120|CM05R120|',
                 '|CH02R120|CM01R120|CM03R120|CM07R120|',
                 '|CH19R120|CM13R120|']

meshLrssi = [   '|MCH07R120|CM01R120|CM13R120|',
                '|CH19R120|CM25R120|',
                '|CH02R120|',
                '|CH04R120|CM03R120|CM05R120|']

meshLinerssi = [    '|MCH03R120|',
                    '|CH02R120|CM01R120|',
                    '|CH04R120|CM05R120|']

meshTrianglerssi = [    '|MCH15R120|CM09R120|CM11R120|CM15R120R120|CM17R120|CM23R120|',
                        '|CH10R120|CM01R120|CM02R120|CM03R120|CM04R120|CM05R120|CM08R120|',
                        '|CH22R120|CM29R120|']

meshPlusrssi    = [     '|MCH15R120|CM14R120|CM16R120|CM21R120|CM27R120|',
                        '|CH09R120|CM13R120|CM17R120|']

def position( i ):
    if( i == 1 ):
        return n2m('01')+'|' 
    elif( i == 2 ):
        return n2m('02')+'|' 
    elif( i == 3 ):
        return n2m('03')+'|' 
    elif( i == 4 ):
        return n2m('04')+'|' 
    elif( i == 5 ):
        return n2m('05')+'|' 
    elif( i == 6 ):
        return n2m('11')+'|' 
    elif( i == 7 ):
        return n2m('17')+'|' 
    elif( i == 8 ):
        return n2m('23')+'|' 
    elif( i == 9 ):
        return n2m('29')+'|'     
    elif( i == 10 ):
        return n2m('28')+'|' 
    elif( i == 11 ):
        return n2m('27')+'|' 
    elif( i == 12 ):
        return n2m('26')+'|' 
    elif( i == 13 ):
        return n2m('25')+'|' 
    elif( i == 14 ):
        return n2m('19')+'|' 
    elif( i == 15 ):
        return n2m('13')+'|' 
    elif( i == 16 ):
        return n2m('07')+'|' 


def sendmessages():
    """Sends cyclic CI messages just like the real testbed."""
    cycle_counter = 0
    position_counter = 1
    colour_change = 1

    while True:

        if stop_event.is_set():
            top.destroy()
            break

        if( mob.get() == 0 ):
            cycle_counter = 0
            position_counter = 1
            colour_change = 1

        if(rssi.get()):
            if(dropvar.get() == "5 by 5"):
                sendstr = mesh5x5rssi
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "2 by 2"):
                sendstr = mesh2x2rssi
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "3 by 3"):
                sendstr = mesh3x3rssi
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "4 by 4"):
                sendstr = mesh4x4rssi
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "Ring"):
                sendstr = meshRingrssi
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "Triangle"):
                sendstr = meshTrianglerssi
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "L"):
                sendstr = meshLrssi
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "Line"):
                sendstr = meshLinerssi
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "Plus"):
                sendstr = meshPlusrssi
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)

        else:
            if(dropvar.get() == "5 by 5"):
                sendstr = mesh5x5
                mob_chkbx.configure(state=NORMAL)
            elif (dropvar.get() == "2 by 2"):
                sendstr = mesh2x2
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "3 by 3"):
                sendstr = mesh3x3
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "4 by 4"):
                sendstr = mesh4x4
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "Ring"):
                sendstr = meshRing
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "Triangle"):
                sendstr = meshTriangle
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "L"):
                sendstr = meshL
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "Line"):
                sendstr = meshLine
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "Plus"):
                sendstr = meshPlus
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)
            elif (dropvar.get() == "6 by 6"):
                sendstr = mesh6x6
                mob_chkbx.deselect()
                mob_chkbx.configure(state=DISABLED)

        for x in range(0, len(sendstr)):
            
            if(start.get()):
                
                if( cycle_counter == 5 ):
                    cycle_counter = 0

                    if( position_counter == 16 ):
                        position_counter = 1
                    else:
                        position_counter = position_counter +1

                    if( position_counter == 1 or position_counter == 16 ):
                        colour_change = 1
                    elif( position_counter == 3 ):
                        colour_change = 2
                    elif( position_counter == 8 ):
                        colour_change = 4
                    elif( position_counter == 11 ):
                        colour_change = 3


                    

                client_socket = socket(AF_INET, SOCK_STREAM)
                client_socket.connect(ADDR)
                timestr = time.strftime("%H:%M:%S", time.gmtime())

                if( mob.get() ):
                    if( x == colour_change ):
                        client_socket.send(bytes(sendstr[x]+n2m('40')+'|', "utf8"))
                        msg_list.insert(tkinter.END, "%s:   %s" % (timestr, sendstr[x]+n2m('40')+'|'))
                    else:
                        msg_list.insert(tkinter.END, "%s:   %s" % (timestr, sendstr[x]))
                        client_socket.send(bytes(sendstr[x], "utf8"))
                else:
                    msg_list.insert(tkinter.END, "%s:   %s" % (timestr, sendstr[x]))
                    client_socket.send(bytes(sendstr[x], "utf8"))
                
                client_socket.close()
                msg_list.see(tkinter.END)
                
                if( x == len(sendstr)-1 ):
                    time.sleep( 0.5 )

                    if( mob.get() ):
                        if( cycle_counter == 0 ):
                            client_socket = socket(AF_INET, SOCK_STREAM)
                            client_socket.connect(ADDR)
                            client_socket.send(bytes(gal40+position(position_counter), "utf8"))
                            client_socket.close()
                            timestr = time.strftime("%H:%M:%S", time.gmtime())
                            msg_list.insert(tkinter.END, "%s:   %s" % (timestr, gal40+position(position_counter)))
                            msg_list.see(tkinter.END)
                        cycle_counter = cycle_counter +1

            else:
                time.sleep( 0.5 )


def on_closing(event=None):
    """This function is to be called when the window is closed."""
    stop_event.set()

top = tkinter.Tk()
top.title("CHaChA Emulator Client")



rssi = tkinter.IntVar()
start = tkinter.IntVar()
mob = tkinter.IntVar()

dropvar = tkinter.StringVar()
dropvar.set("5 by 5")

messages_frame = tkinter.Frame(top)
my_msg = tkinter.StringVar() 
scrollbar = tkinter.Scrollbar(messages_frame)
msg_list = tkinter.Listbox(messages_frame, height=25, width=170, yscrollcommand=scrollbar.set)
scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
msg_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
msg_list.pack()
messages_frame.pack()

start_chkbx = tkinter.Checkbutton(top, text="RUN", variable=start)
start_chkbx.pack(in_=top, side=tkinter.LEFT)

ddlabel = tkinter.Label(top, text="       topology:")
ddlabel.pack(in_=top, side=tkinter.LEFT)

dropdown = tkinter.OptionMenu(top, dropvar, "5 by 5", "4 by 4", "3 by 3", "2 by 2", "Ring", "Triangle", "L", "Line", "Plus", "6 by 6")
dropdown.pack(in_=top, side=tkinter.LEFT)

rssi_chkbx = tkinter.Checkbutton(top, text="RSSI (currently unused)", variable=rssi)
rssi_chkbx.pack(in_=top, side=tkinter.LEFT)
rssi_chkbx.configure(state=DISABLED)

mob_chkbx = tkinter.Checkbutton(top, text="travelling GAL40", variable=mob)
mob_chkbx.pack(in_=top, side=tkinter.RIGHT)


top.protocol("WM_DELETE_WINDOW", on_closing)

BUFSIZ = 1024
ADDR = (HOST, PORT)

client_socket = socket(AF_INET, SOCK_STREAM)

stop_event = threading.Event()

ch_thread = threading.Thread(target=sendmessages)
ch_thread.setDaemon(True)
ch_thread.start()

top.mainloop()  # Starts GUI execution.

