package global;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.awt.*;


class Ball {
	  int x,y,radius,dx,dy;
	  Color BallColor;

	  public Ball(int x,int y,int radius,int dx,int dy,Color bColor)
	  {
	  this.x=x;
	  this.y=y;
	  this.radius=radius;
	  this.dx=dx;
	  this.dy=dy;	
	  BallColor=bColor;	
	  }
}

class CH {
	int X,Y,Length;
	Color CHColor;
	
	public CH(int x, int y, int length, Color chcolor)
	{
		this.X=x;
		this.Y=y;
		this.Length=length;
		CHColor=chcolor;
	}
	
	public void del()
	{
		this.X=0;
		this.Y=0;
		this.Length=0;
		CHColor=Color.white;
	}
}


public class CHaChAMonitor extends Panel implements Runnable{

	Timer timer;

	private static final long serialVersionUID = 1L;
	String s = "CHaChA Monitor";
	int x = 20;
	int y = 50;

	public static void main(String[] args) {
		Frame f = new Frame("CHaChA Monitor");
		f.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				System.exit(0);
			};
		});

		CHaChAMonitor ut = new CHaChAMonitor();
		ut.setSize(800,800);
		f.add(ut);
		f.pack();
		ut.init();
		f.setSize(700,850 + 20); // add 20 for the frame title
		f.setVisible(true);
	}

	Ball GAL01, GAL02, GAL03, GAL04, GAL05, GAL06, GAL07, GAL08, GAL09, GAL10, GAL11, GAL12, GAL13, GAL14, GAL15, GAL16, GAL17, GAL18, GAL19, GAL20, GAL21, GAL22, GAL23, GAL24, GAL25, GAL26, GAL27, GAL28, GAL29, GAL30, GAL31, GAL32, GAL33, GAL34, GAL35, GAL36, GALEXP;
	CH CH01, CH02, CH03, CH04, CH05, CH06, CH07, CH08, CH09, CH10, CH11, CH12, CH13, CH14, CH15, CH16, CH17, CH18, CH19, CH20, CH21, CH22, CH23, CH24, CH25, CH26, CH27, CH28, CH29, CH30, CH31, CH32, CH33, CH34, CH35, CH36, CHEXP;
	
	public void init()
	{
		globalClusterInfo = new ArrayList<ArrayList<String>>();
		resetDeletedNodes = new ArrayList<String>();
		allClusterChannels = new ArrayList<String>();
		incomingCIMessageTimeStamps = new ArrayList<Integer>();
		clusteringDoneList = new ArrayList<String>();
		
		statusString = "Status: CHaChA Monitor was started";
		startTimeString = "CHaChA Monitor was started at: " + Util.getCurrentTime();
		
		gal40position = new int[2];
		
//		startTimeInt = (int)System.currentTimeMillis()/1000;
//		currentTimeInt = (int)System.currentTimeMillis()/1000;
		
//		elapsedTimeString = "Elapsed Time: " + Integer.toString(currentTimeInt - startTimeInt) + " Seconds";
		
		roamingFlag = false;
		
		setBackground(Color.white);
		
		createEmptyGrid();
		createEmptyCH();
		
	    Thread t = new Thread(this);
	    t.start();
	}
	
	public void centerString(Graphics g, String s, Font font) 
	{
	    FontRenderContext frc = new FontRenderContext(null, true, true);

	    Rectangle2D r2D = font.getStringBounds(s, frc);
	    
	    int rWidth = (int) Math.round(r2D.getWidth());
	    int rHeight = (int) Math.round(r2D.getHeight());
	    
	    int rX = (int) Math.round(r2D.getX());
	    int rY = (int) Math.round(r2D.getY());

	    int a = (rWidth / 2) - rX;
	    int b = (rHeight / 2) - rY;

	    g.setFont(font);
	    g.drawString(s, a, b);
	}
	
	public void paint(Graphics g)
	{
		Font currentFont = g.getFont();
		g.setColor(Color.black);
		Font newFont = currentFont.deriveFont(currentFont.getSize() * 1.4F);
		
		g.drawLine(0, 640, 700, 640);
		
		g.setFont(newFont);
		g.drawString(statusString, 70, 695);
		
		g.drawString(startTimeString, 70, 740);
		
//		currentTimeInt = (int)System.currentTimeMillis()/1000;
//		elapsedTimeString = "Elapsed Time: " + Integer.toString(currentTimeInt - startTimeInt) + " Seconds";
//		g.drawString(elapsedTimeString, 70, 765);
		
		g.setColor(GAL01.BallColor);
		g.fillOval(GAL01.x, GAL01.y, GAL01.radius, GAL01.radius);
		
		g.setColor(GAL02.BallColor);
		g.fillOval(GAL02.x, GAL02.y, GAL02.radius, GAL02.radius);
		
		g.setColor(GAL03.BallColor);
		g.fillOval(GAL03.x, GAL03.y, GAL03.radius, GAL03.radius);
		
		g.setColor(GAL04.BallColor);
		g.fillOval(GAL04.x, GAL04.y, GAL04.radius, GAL04.radius);
		
		g.setColor(GAL05.BallColor);
		g.fillOval(GAL05.x, GAL05.y, GAL05.radius, GAL05.radius);
		
		g.setColor(GAL06.BallColor);
		g.fillOval(GAL06.x, GAL06.y, GAL06.radius, GAL06.radius);
		
		g.setColor(GAL07.BallColor);
		g.fillOval(GAL07.x, GAL07.y, GAL07.radius, GAL07.radius);
		
		g.setColor(GAL08.BallColor);
		g.fillOval(GAL08.x, GAL08.y, GAL08.radius, GAL08.radius);
		
		g.setColor(GAL09.BallColor);
		g.fillOval(GAL09.x, GAL09.y, GAL09.radius, GAL09.radius);
		
		g.setColor(GAL10.BallColor);
		g.fillOval(GAL10.x, GAL10.y, GAL10.radius, GAL10.radius);
		
		g.setColor(GAL11.BallColor);
		g.fillOval(GAL11.x, GAL11.y, GAL11.radius, GAL11.radius);
		
		g.setColor(GAL12.BallColor);
		g.fillOval(GAL12.x, GAL12.y, GAL12.radius, GAL12.radius);
		
		g.setColor(GAL13.BallColor);
		g.fillOval(GAL13.x, GAL13.y, GAL13.radius, GAL13.radius);
		
		g.setColor(GAL14.BallColor);
		g.fillOval(GAL14.x, GAL14.y, GAL14.radius, GAL14.radius);

		g.setColor(GAL15.BallColor);
		g.fillOval(GAL15.x, GAL15.y, GAL15.radius, GAL15.radius);
		
		g.setColor(GAL16.BallColor);
		g.fillOval(GAL16.x, GAL16.y, GAL16.radius, GAL16.radius);
		
		g.setColor(GAL17.BallColor);
		g.fillOval(GAL17.x, GAL17.y, GAL17.radius, GAL17.radius);
		
		g.setColor(GAL18.BallColor);
		g.fillOval(GAL18.x, GAL18.y, GAL18.radius, GAL18.radius);
		
		g.setColor(GAL19.BallColor);
		g.fillOval(GAL19.x, GAL19.y, GAL19.radius, GAL19.radius);
		
		g.setColor(GAL20.BallColor);
		g.fillOval(GAL20.x, GAL20.y, GAL20.radius, GAL20.radius);
		
		g.setColor(GAL21.BallColor);
		g.fillOval(GAL21.x, GAL21.y, GAL21.radius, GAL21.radius);
		
		g.setColor(GAL22.BallColor);
		g.fillOval(GAL22.x, GAL22.y, GAL22.radius, GAL22.radius);
		
		g.setColor(GAL23.BallColor);
		g.fillOval(GAL23.x, GAL23.y, GAL23.radius, GAL23.radius);
		
		g.setColor(GAL24.BallColor);
		g.fillOval(GAL24.x, GAL24.y, GAL24.radius, GAL24.radius);
		
		g.setColor(GAL25.BallColor);
		g.fillOval(GAL25.x, GAL25.y, GAL25.radius, GAL25.radius);
		
		g.setColor(GAL26.BallColor);
		g.fillOval(GAL26.x, GAL26.y, GAL26.radius, GAL26.radius);
		
		g.setColor(GAL27.BallColor);
		g.fillOval(GAL27.x, GAL27.y, GAL27.radius, GAL27.radius);
		
		g.setColor(GAL28.BallColor);
		g.fillOval(GAL28.x, GAL28.y, GAL28.radius, GAL28.radius);
		
		g.setColor(GAL29.BallColor);
		g.fillOval(GAL29.x, GAL29.y, GAL29.radius, GAL29.radius);
		
		g.setColor(GAL30.BallColor);
		g.fillOval(GAL30.x, GAL30.y, GAL30.radius, GAL30.radius);
		
		g.setColor(GAL31.BallColor);
		g.fillOval(GAL31.x, GAL31.y, GAL31.radius, GAL31.radius);
		
		g.setColor(GAL32.BallColor);
		g.fillOval(GAL32.x, GAL32.y, GAL32.radius, GAL32.radius);
		
		g.setColor(GAL33.BallColor);
		g.fillOval(GAL33.x, GAL33.y, GAL33.radius, GAL33.radius);
		
		g.setColor(GAL34.BallColor);
		g.fillOval(GAL34.x, GAL34.y, GAL34.radius, GAL34.radius);
		
		g.setColor(GAL35.BallColor);
		g.fillOval(GAL35.x, GAL35.y, GAL35.radius, GAL35.radius);
		
		g.setColor(GAL36.BallColor);
		g.fillOval(GAL36.x, GAL36.y, GAL36.radius, GAL36.radius);
		
		g.setColor(GALEXP.BallColor);
		g.fillOval(GALEXP.x, GALEXP.y, GALEXP.radius, GALEXP.radius);
		
		g.setColor(CH01.CHColor);
		g.drawRect(CH01.X, CH01.Y, CH01.Length, CH01.Length);
		
		g.setColor(CH02.CHColor);
		g.drawRect(CH02.X, CH02.Y, CH02.Length, CH02.Length);
		
		g.setColor(CH03.CHColor);
		g.drawRect(CH03.X, CH03.Y, CH03.Length, CH03.Length);
		
		g.setColor(CH04.CHColor);
		g.drawRect(CH04.X, CH04.Y, CH04.Length, CH04.Length);
		
		g.setColor(CH05.CHColor);
		g.drawRect(CH05.X, CH05.Y, CH05.Length, CH05.Length);
		
		g.setColor(CH06.CHColor);
		g.drawRect(CH06.X, CH06.Y, CH06.Length, CH06.Length);
		
		g.setColor(CH07.CHColor);
		g.drawRect(CH07.X, CH07.Y, CH07.Length, CH07.Length);
		
		g.setColor(CH08.CHColor);
		g.drawRect(CH08.X, CH08.Y, CH08.Length, CH08.Length);
		
		g.setColor(CH09.CHColor);
		g.drawRect(CH09.X, CH09.Y, CH09.Length, CH09.Length);
		
		g.setColor(CH10.CHColor);
		g.drawRect(CH10.X, CH10.Y, CH10.Length, CH10.Length);
		
		g.setColor(CH11.CHColor);
		g.drawRect(CH11.X, CH11.Y, CH11.Length, CH11.Length);
		
		g.setColor(CH12.CHColor);
		g.drawRect(CH12.X, CH12.Y, CH12.Length, CH12.Length);
		
		g.setColor(CH13.CHColor);
		g.drawRect(CH13.X, CH13.Y, CH13.Length, CH13.Length);
		
		g.setColor(CH14.CHColor);
		g.drawRect(CH14.X, CH14.Y, CH14.Length, CH14.Length);
		
		g.setColor(CH15.CHColor);
		g.drawRect(CH15.X, CH15.Y, CH15.Length, CH15.Length);
		
		g.setColor(CH16.CHColor);
		g.drawRect(CH16.X, CH16.Y, CH16.Length, CH16.Length);
		
		g.setColor(CH17.CHColor);
		g.drawRect(CH17.X, CH17.Y, CH17.Length, CH17.Length);
		
		g.setColor(CH18.CHColor);
		g.drawRect(CH18.X, CH18.Y, CH18.Length, CH18.Length);
		
		g.setColor(CH19.CHColor);
		g.drawRect(CH19.X, CH19.Y, CH19.Length, CH19.Length);
		
		g.setColor(CH20.CHColor);
		g.drawRect(CH20.X, CH20.Y, CH20.Length, CH20.Length);
		
		g.setColor(CH21.CHColor);
		g.drawRect(CH21.X, CH21.Y, CH21.Length, CH21.Length);
		
		g.setColor(CH22.CHColor);
		g.drawRect(CH22.X, CH22.Y, CH22.Length, CH22.Length);
		
		g.setColor(CH23.CHColor);
		g.drawRect(CH23.X, CH23.Y, CH23.Length, CH23.Length);
		
		g.setColor(CH24.CHColor);
		g.drawRect(CH24.X, CH24.Y, CH24.Length, CH24.Length);
		
		g.setColor(CH25.CHColor);
		g.drawRect(CH25.X, CH25.Y, CH25.Length, CH25.Length);
		
		g.setColor(CH26.CHColor);
		g.drawRect(CH26.X, CH26.Y, CH26.Length, CH26.Length);
		
		g.setColor(CH27.CHColor);
		g.drawRect(CH27.X, CH27.Y, CH27.Length, CH27.Length);
		
		g.setColor(CH28.CHColor);
		g.drawRect(CH28.X, CH28.Y, CH28.Length, CH28.Length);
		
		g.setColor(CH29.CHColor);
		g.drawRect(CH29.X, CH29.Y, CH29.Length, CH29.Length);
		
		g.setColor(CH30.CHColor);
		g.drawRect(CH30.X, CH30.Y, CH30.Length, CH30.Length);
		
		g.setColor(CH31.CHColor);
		g.drawRect(CH31.X, CH31.Y, CH31.Length, CH31.Length);
		
		g.setColor(CH32.CHColor);
		g.drawRect(CH32.X, CH32.Y, CH32.Length, CH32.Length);
		
		g.setColor(CH33.CHColor);
		g.drawRect(CH33.X, CH33.Y, CH33.Length, CH33.Length);
		
		g.setColor(CH34.CHColor);
		g.drawRect(CH34.X, CH34.Y, CH34.Length, CH34.Length);
		
		g.setColor(CH35.CHColor);
		g.drawRect(CH35.X, CH35.Y, CH35.Length, CH35.Length);
		
		g.setColor(CH36.CHColor);
		g.drawRect(CH36.X, CH36.Y, CH36.Length, CH36.Length);
		
		g.setColor(CHEXP.CHColor);
		g.drawRect(CHEXP.X, CHEXP.Y, CHEXP.Length, CHEXP.Length);
	}
	
	public void run()
	{
		// TODO: retrieve main Ethernet interface IP on Windows/Linux
//		try
//		{
//			myEthIfName = "eth0";
//			
//			Scanner scan = ProcessCall.runCommandWithOutput("./ifconfig", GlobalDefines.toolPath, myEthIfName);
//			String mod;
//			while (!(mod=scan.nextLine()).contains("inet"));
//			mod = mod.trim();
//			
//			myEthernetIP = mod.substring(mod.indexOf("inet"), mod.indexOf("Bcast")-1).split(":")[1].trim();
//			
//			System.out.println(Util.getCurrentTime() +  ": My Ethernet IP: " + myEthernetIP);
//			
//		} catch(IndexOutOfBoundsException e)
//		{
//			e.printStackTrace();
//		}
		
		try
		{
//			receiveTCPSocket = new ServerSocket(GlobalDefines.TCP_ETHERNET_RECEIVE_PORT, 50, InetAddress.getByName(myEthernetIP));
			receiveTCPSocket = new ServerSocket(GlobalDefines.TCP_ETHERNET_RECEIVE_PORT, 50, InetAddress.getByName(GlobalDefines.ETHERNET_IP));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		receiverTCP = new Runnable()
		{
			@Override
			public void run()
			{
				while (true)
				{
					try
					{
						final Socket client = receiveTCPSocket.accept();
						
						new Thread() {
							@Override
							public void run()
							{
								super.run();
								
								try
								{
									Scanner in = new Scanner( client.getInputStream() );
									handleReceivedCommand(client.getInetAddress().getHostAddress(), new String(in.nextLine()));
									in.close();
							    	client.close();
									
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}.start();
						
					} catch ( IOException e ) {
						e.printStackTrace();
					}
				}
			}
		};
		
		new Thread(receiverTCP).start();
		
	    while(true)
	    {
	    	try {
	    		
	    		timeStamp = Util.getTimeStampInSeconds();
	    		
	    		// TODO: work on current snapshot of data structures in the following functions
	    		
	    		checkCIMessageTimeStamps();
	    		createEmptyGrid();
				createEmptyCH();
	    		setBallColors();
	    		updateStatus();
	    		if( RepaintFlag )
	    		{
	    			RepaintFlag = false;
	    			repaint();
	    			startTimeString = "Last update: " + Util.getCurrentTime();
	    		}
	    		Thread.sleep(200);
	    	}
	    	catch(Exception e)
	    	{
	    		
	    	}
	    }
	}
	
	public void updateStatus()
	{
		if( !clusteringDoneList.isEmpty() )
		{
//			roamingFlag = true; // TODO
			
			if( clusteringDoneList.size() == globalClusterInfo.size() )
			{
				statusString = "Status: Clustering is done, roaming starts";
			}
			else {
				if( globalClusterInfo.size() > clusteringDoneList.size() )
				{
					statusString = "Status: Waiting for " + Integer.toString(globalClusterInfo.size()-clusteringDoneList.size()) + " further ClusteringDone messages";
				}
				else {
					statusString = "Status: Listening for incoming messages";
				}
			}
		}
	}
	
	public void setBallColors()
	{
		String CurrentChannel = "";
		int xx = 0;
		int yy = 0;
		
		if( !resetDeletedNodes.isEmpty() )
		{
			for( int i = 0; i < resetDeletedNodes.size(); i++ )
			{
				if( !resetDeletedNodes.get(i).equals(GlobalDefines.EXP_NODE) )
				{
					xx = getBallPositions(resetDeletedNodes.get(i),5,30,70 )[0];
					yy = getBallPositions(resetDeletedNodes.get(i),5,30,70 )[1];
//					getBallBasedOnMac(resetDeletedNodes.get(i)).radius = 5;
					getBallBasedOnMac(resetDeletedNodes.get(i)).x = xx;
					getBallBasedOnMac(resetDeletedNodes.get(i)).y = yy;
				}
				else {
					setGAL40Position(lastGal40NH2CH, 30, 30, 70);
					GALEXP.radius = 30;
				}
				
				getBallBasedOnMac(resetDeletedNodes.get(i)).BallColor = Color.black;
				getCHBasedOnMac(resetDeletedNodes.get(i)).CHColor = Color.white;
			}
			
			resetDeletedNodes.clear();
		}
				
		for( int i = 0; i < globalClusterInfo.size(); i++ )
		{
			CurrentChannel = allClusterChannels.get(i);
			
			getCHBasedOnMac(globalClusterInfo.get(i).get(0)).CHColor = Color.black;
			
			for( int j = 0; j < globalClusterInfo.get(i).size(); j++ )
			{
				getBallBasedOnMac(globalClusterInfo.get(i).get(j)).BallColor = getChannelColor(CurrentChannel);
				
				if( !globalClusterInfo.get(i).get(j).equals(GlobalDefines.EXP_NODE) )
				{
					if( getChannelColor(CurrentChannel) != Color.black )
					{
						xx = getBallPositions(globalClusterInfo.get(i).get(j),30,30,70 )[0];
						yy = getBallPositions(globalClusterInfo.get(i).get(j),30,30,70 )[1];
						getBallBasedOnMac(globalClusterInfo.get(i).get(j)).radius = 30;
						getBallBasedOnMac(globalClusterInfo.get(i).get(j)).x = xx;
						getBallBasedOnMac(globalClusterInfo.get(i).get(j)).y = yy;
					}
					else {
						xx = getBallPositions(globalClusterInfo.get(i).get(j),5,30,70 )[0];
						yy = getBallPositions(globalClusterInfo.get(i).get(j),5,30,70 )[1];
						getBallBasedOnMac(globalClusterInfo.get(i).get(j)).radius = 5;
						getBallBasedOnMac(globalClusterInfo.get(i).get(j)).x = xx;
						getBallBasedOnMac(globalClusterInfo.get(i).get(j)).y = yy;
					}
				}
				else {
					if( getChannelColor(CurrentChannel) != Color.black )
					{
						getBallBasedOnMac(globalClusterInfo.get(i).get(j)).x = gal40position[0];
						getBallBasedOnMac(globalClusterInfo.get(i).get(j)).y = gal40position[1];
					}
					else {
						setGAL40Position(lastGal40NH2CH, 5, 30, 70);
//						GALEXP.radius = 5;
					}
				}
			}
		}		
	}
	
	public int[] getBallPositions( String mac, int radius , int maxradius , int distance )
	{	
		int[] xy = new int[2];
		
		switch (mac)
		{
			case "04:f0:21:10:c3:23": // Galileo 1
				xy[0] = 2*distance + ((maxradius-radius)/2);
				xy[1] = (7*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:20": // Galileo 2
				xy[0] = 3*distance + ((maxradius-radius)/2);
				xy[1] = (7*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c8:0f": // Galileo 3
				xy[0] = 4*distance + ((maxradius-radius)/2);
				xy[1] = (7*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:4f": // Galileo 4
				xy[0] = 5*distance + ((maxradius-radius)/2);
				xy[1] = (7*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c2:90": // Galileo 5
				xy[0] = 6*distance + ((maxradius-radius)/2);
				xy[1] = (7*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c2:8a": // Galileo 6
				xy[0] = 7*distance + ((maxradius-radius)/2);
				xy[1] = (7*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c8:18": // Galileo 7
				xy[0] = 2*distance + ((maxradius-radius)/2);
				xy[1] = (6*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:2b": // Galileo 8
				xy[0] = 3*distance + ((maxradius-radius)/2);
				xy[1] = (6*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c8:15": // Galileo 9
				xy[0] = 4*distance + ((maxradius-radius)/2);
				xy[1] = (6*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:57": // Galileo 10
				xy[0] = 5*distance + ((maxradius-radius)/2);
				xy[1] = (6*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:41": // Galileo 11
				xy[0] = 6*distance + ((maxradius-radius)/2);
				xy[1] = (6*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:71": // Galileo 12
				xy[0] = 7*distance + ((maxradius-radius)/2);
				xy[1] = (6*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:2a": // Galileo 13
				xy[0] = 2*distance + ((maxradius-radius)/2);
				xy[1] = (5*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:62": // Galileo 14
				xy[0] = 3*distance + ((maxradius-radius)/2);
				xy[1] = (5*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:55": // Galileo 15
				xy[0] = 4*distance + ((maxradius-radius)/2);
				xy[1] = (5*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c2:8e": // Galileo 16
				xy[0] = 5*distance + ((maxradius-radius)/2);
				xy[1] = (5*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:0f:77:54": // Galileo 17
				xy[0] = 6*distance + ((maxradius-radius)/2);
				xy[1] = (5*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:39": // Galileo 18
				xy[0] = 7*distance + ((maxradius-radius)/2);
				xy[1] = (5*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:33": // Galileo 19
				xy[0] = 2*distance + ((maxradius-radius)/2);
				xy[1] = (4*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c5:af": // Galileo 20
				xy[0] = 3*distance + ((maxradius-radius)/2);
				xy[1] = (4*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:3f": // Galileo 21
				xy[0] = 4*distance + ((maxradius-radius)/2);
				xy[1] = (4*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:51": // Galileo 22
				xy[0] = 5*distance + ((maxradius-radius)/2);
				xy[1] = (4*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:4c": // Galileo 23
				xy[0] = 6*distance + ((maxradius-radius)/2);
				xy[1] = (4*distance) + ((maxradius-radius)/2);
				break; 
				
			case "04:f0:21:10:c2:88": // Galileo 24
				xy[0] = 7*distance + ((maxradius-radius)/2);
				xy[1] = (4*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c8:0d": // Galileo 25
				xy[0] = 2*distance + ((maxradius-radius)/2);
				xy[1] = (3*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:53": // Galileo 26
				xy[0] = 3*distance + ((maxradius-radius)/2);
				xy[1] = (3*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c5:52": // Galileo 27
				xy[0] = 4*distance + ((maxradius-radius)/2);
				xy[1] = (3*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c8:17": // Galileo 28
				xy[0] = 5*distance + ((maxradius-radius)/2);
				xy[1] = (3*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c5:51": // Galileo 29
				xy[0] = 6*distance + ((maxradius-radius)/2);
				xy[1] = (3*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:59": // Galileo 30
				xy[0] = 7*distance + ((maxradius-radius)/2);
				xy[1] = (3*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:52": // Galileo 31
				xy[0] = 2*distance + ((maxradius-radius)/2);
				xy[1] = (2*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:54": // Galileo 32
				xy[0] = 3*distance + ((maxradius-radius)/2);
				xy[1] = (2*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:38": // Galileo 33
				xy[0] = 4*distance + ((maxradius-radius)/2);
				xy[1] = (2*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:65": // Galileo 34
				xy[0] = 5*distance + ((maxradius-radius)/2);
				xy[1] = (2*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:21": // Galileo 35
				xy[0] = 6*distance + ((maxradius-radius)/2);
				xy[1] = (2*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c5:53": // Galileo 36
				xy[0] = 7*distance + ((maxradius-radius)/2);
				xy[1] = (2*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:14:c6:5e": // Galileo 40 (GALEXP)
				xy[0] = distance + ((maxradius-radius)/2);
				xy[1] = 8*(distance) + ((maxradius-radius)/2);
				break;
			
			default: // unknown
				break;
		}
		return xy;
	}

	public void setGAL40Position( String mac, int radius , int maxradius , int distance )
	{			
		int[] xy = new int[2];
		
		switch (mac)
		{
			case "04:f0:21:10:c3:23": // Galileo 1
				xy[0] = 1*distance + ((maxradius-radius)/2);
				xy[1] = (8*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:20": // Galileo 2
				xy[0] = 3*distance + ((maxradius-radius)/2);
				xy[1] = (8*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c8:0f": // Galileo 3
				xy[0] = 4*distance + ((maxradius-radius)/2);
				xy[1] = (8*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:4f": // Galileo 4
				xy[0] = 5*distance + ((maxradius-radius)/2);
				xy[1] = (8*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c2:90": // Galileo 5
				xy[0] = 8*distance + ((maxradius-radius)/2);
				xy[1] = (8*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c8:18": // Galileo 7
				xy[0] = 1*distance + ((maxradius-radius)/2);
				xy[1] = (6*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:41": // Galileo 11
				xy[0] = 8*distance + ((maxradius-radius)/2);
				xy[1] = (6*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:2a": // Galileo 13
				xy[0] = 1*distance + ((maxradius-radius)/2);
				xy[1] = (5*distance) + ((maxradius-radius)/2);
				break;

			case "04:f0:21:0f:77:54": // Galileo 17
				xy[0] = 8*distance + ((maxradius-radius)/2);
				xy[1] = (5*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:33": // Galileo 19
				xy[0] = 1*distance + ((maxradius-radius)/2);
				xy[1] = (4*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:4c": // Galileo 23
				xy[0] = 8*distance + ((maxradius-radius)/2);
				xy[1] = (4*distance) + ((maxradius-radius)/2);
				break; 
	
			case "04:f0:21:10:c8:0d": // Galileo 25
				xy[0] = 1*distance + ((maxradius-radius)/2);
				xy[1] = (1*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c3:53": // Galileo 26
				xy[0] = 3*distance + ((maxradius-radius)/2);
				xy[1] = (1*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c5:52": // Galileo 27
				xy[0] = 4*distance + ((maxradius-radius)/2);
				xy[1] = (1*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c8:17": // Galileo 28
				xy[0] = 5*distance + ((maxradius-radius)/2);
				xy[1] = (1*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:10:c5:51": // Galileo 29
				xy[0] = 8*distance + ((maxradius-radius)/2);
				xy[1] = (1*distance) + ((maxradius-radius)/2);
				break;
				
			case "04:f0:21:14:c6:5e": // Galileo 40 (GALEXP)
				xy[0] = 6*distance + ((maxradius-radius)/2);
				xy[1] = 15*(distance)/2 + ((maxradius-radius)/2);
				break;
			
			default: // unknown
				break;
		}
		
		gal40position = xy;
		
		GALEXP.x = gal40position[0];
		GALEXP.y = gal40position[1];
	}
	
	public Color getChannelColor( String Channel )
	{
		Color ChannelColor = Color.black;
		
		Color thirtysix = new Color(65, 105, 225);
		Color forty = new Color( 255, 0, 0 );
		Color fortyfour = new Color( 0, 255, 0 );
		Color fortyeight = new Color( 255, 193, 37 );
		Color hundredfiftythree = new Color( 255, 20, 147 );
		Color hundredfiftyseven = new Color( 165, 42, 42 );
		
		switch(Channel)
		{
			case  "36": {
				ChannelColor =  thirtysix;
			}
			break;
			
			case  "40": {
				ChannelColor = forty;
			}
			break;
			
			case  "44": {
				ChannelColor = fortyfour;
			}
			break;
			
			case  "48": {
				ChannelColor = fortyeight;
			}
			break;
			
			case  "153": {
				ChannelColor = hundredfiftythree;
			}
			break;
			
			case  "157": {
				ChannelColor = hundredfiftyseven;
			}
			break;
		}
		
		return ChannelColor;
	}
	
	public Ball getBallBasedOnMac( String mac ) {
		Ball ch = new Ball(0, 0, 0, 0, 0, Color.white);
		
		switch (mac)
		{
			case "04:f0:21:10:c3:23": // Galileo 1
				ch = GAL01;
				break;
				
			case "04:f0:21:10:c3:20": // Galileo 2
				ch = GAL02;
				break;
				
			case "04:f0:21:10:c8:0f": // Galileo 3
				ch = GAL03;
				break;
				
			case "04:f0:21:10:c3:4f": // Galileo 4
				ch = GAL04;
				break;
				
			case "04:f0:21:10:c2:90": // Galileo 5
				ch = GAL05;
				break;
				
			case "04:f0:21:10:c2:8a": // Galileo 6
				ch = GAL06;
				break;
				
			case "04:f0:21:10:c8:18": // Galileo 7
				ch = GAL07;
				break;
				
			case "04:f0:21:10:c3:2b": // Galileo 8
				ch = GAL08;
				break;
				
			case "04:f0:21:10:c8:15": // Galileo 9
				ch = GAL09;
				break;
				
			case "04:f0:21:10:c3:57": // Galileo 10
				ch = GAL10;
				break;
				
			case "04:f0:21:10:c3:41": // Galileo 11
				ch = GAL11;
				break;
				
			case "04:f0:21:10:c3:71": // Galileo 12
				ch = GAL12;
				break;
				
			case "04:f0:21:10:c3:2a": // Galileo 13
				ch = GAL13;
				break;
				
			case "04:f0:21:10:c3:62": // Galileo 14
				ch = GAL14;
				break;
				
			case "04:f0:21:10:c3:55": // Galileo 15
				ch = GAL15;
				break;
				
			case "04:f0:21:10:c2:8e": // Galileo 16
				ch = GAL16;
				break;
				
			case "04:f0:21:0f:77:54": // Galileo 17
				ch = GAL17;
				break;
				
			case "04:f0:21:10:c3:39": // Galileo 18
				ch = GAL18;
				break;
				
			case "04:f0:21:10:c3:33": // Galileo 19
				ch = GAL19;
				break;
				
			case "04:f0:21:10:c5:af": // Galileo 20
				ch = GAL20;
				break;
				
			case "04:f0:21:10:c3:3f": // Galileo 21
				ch = GAL21;
				break;
				
			case "04:f0:21:10:c3:51": // Galileo 22
				ch = GAL22;
				break;
				
			case "04:f0:21:10:c3:4c": // Galileo 23
				ch = GAL23;
				break; 
				
			case "04:f0:21:10:c2:88": // Galileo 24
				ch = GAL24;
				break;
				
			case "04:f0:21:10:c8:0d": // Galileo 25
				ch = GAL25;
				break;
				
			case "04:f0:21:10:c3:53": // Galileo 26
				ch = GAL26;
				break;
				
			case "04:f0:21:10:c5:52": // Galileo 27
				ch = GAL27;
				break;
				
			case "04:f0:21:10:c8:17": // Galileo 28
				ch = GAL28;
				break;
				
			case "04:f0:21:10:c5:51": // Galileo 29
				ch = GAL29;
				break;
				
			case "04:f0:21:10:c3:59": // Galileo 30
				ch = GAL30;
				break;
				
			case "04:f0:21:10:c3:52": // Galileo 31
				ch = GAL31;
				break;
				
			case "04:f0:21:10:c3:54": // Galileo 32
				ch = GAL32;
				break;
				
			case "04:f0:21:10:c3:38": // Galileo 33
				ch = GAL33;
				break;
				
			case "04:f0:21:10:c3:65": // Galileo 34
				ch = GAL34;
				break;
				
			case "04:f0:21:10:c3:21": // Galileo 35
				ch = GAL35;
				break;
				
			case "04:f0:21:10:c5:53": // Galileo 36
				ch = GAL36;
				break;
				
			case "04:f0:21:14:c6:5e": // Galileo 40 (GALEXP)
				ch = GALEXP;
				break;
			
			default: // unknown
				break;
		}
		
		return ch;
	}
	
	public CH getCHBasedOnMac( String mac ) {
		CH ch = new CH(0, 0, 0, Color.white);
		
		switch (mac)
		{
			case "04:f0:21:10:c3:23": // Galileo 1
				ch = CH01;
				break;
				
			case "04:f0:21:10:c3:20": // Galileo 2
				ch = CH02;
				break;
				
			case "04:f0:21:10:c8:0f": // Galileo 3
				ch = CH03;
				break;
				
			case "04:f0:21:10:c3:4f": // Galileo 4
				ch = CH04;
				break;
				
			case "04:f0:21:10:c2:90": // Galileo 5
				ch = CH05;
				break;
				
			case "04:f0:21:10:c2:8a": // Galileo 6
				ch = CH06;
				break;
				
			case "04:f0:21:10:c8:18": // Galileo 7
				ch = CH07;
				break;
				
			case "04:f0:21:10:c3:2b": // Galileo 8
				ch = CH08;
				break;
				
			case "04:f0:21:10:c8:15": // Galileo 9
				ch = CH09;
				break;
				
			case "04:f0:21:10:c3:57": // Galileo 10
				ch = CH10;
				break;
				
			case "04:f0:21:10:c3:41": // Galileo 11
				ch = CH11;
				break;
				
			case "04:f0:21:10:c3:71": // Galileo 12
				ch = CH12;
				break;
				
			case "04:f0:21:10:c3:2a": // Galileo 13
				ch = CH13;
				break;
				
			case "04:f0:21:10:c3:62": // Galileo 14
				ch = CH14;
				break;
				
			case "04:f0:21:10:c3:55": // Galileo 15
				ch = CH15;
				break;
				
			case "04:f0:21:10:c2:8e": // Galileo 16
				ch = CH16;
				break;
				
			case "04:f0:21:0f:77:54": // Galileo 17
				ch = CH17;
				break;
				
			case "04:f0:21:10:c3:39": // Galileo 18
				ch = CH18;
				break;
				
			case "04:f0:21:10:c3:33": // Galileo 19
				ch = CH19;
				break;
				
			case "04:f0:21:10:c5:af": // Galileo 20
				ch = CH20;
				break;
				
			case "04:f0:21:10:c3:3f": // Galileo 21
				ch = CH21;
				break;
				
			case "04:f0:21:10:c3:51": // Galileo 22
				ch = CH22;
				break;
				
			case "04:f0:21:10:c3:4c": // Galileo 23
				ch = CH23;
				break; 
				
			case "04:f0:21:10:c2:88": // Galileo 24
				ch = CH24;
				break;
				
			case "04:f0:21:10:c8:0d": // Galileo 25
				ch = CH25;
				break;
				
			case "04:f0:21:10:c3:53": // Galileo 26
				ch = CH26;
				break;
				
			case "04:f0:21:10:c5:52": // Galileo 27
				ch = CH27;
				break;
				
			case "04:f0:21:10:c8:17": // Galileo 28
				ch = CH28;
				break;
				
			case "04:f0:21:10:c5:51": // Galileo 29
				ch = CH29;
				break;
				
			case "04:f0:21:10:c3:59": // Galileo 30
				ch = CH30;
				break;
				
			case "04:f0:21:10:c3:52": // Galileo 31
				ch = CH31;
				break;
				
			case "04:f0:21:10:c3:54": // Galileo 32
				ch = CH32;
				break;
				
			case "04:f0:21:10:c3:38": // Galileo 33
				ch = CH33;
				break;
				
			case "04:f0:21:10:c3:65": // Galileo 34
				ch = CH34;
				break;
				
			case "04:f0:21:10:c3:21": // Galileo 35
				ch = CH35;
				break;
				
			case "04:f0:21:10:c5:53": // Galileo 36
				ch = CH36;
				break;
				
			case "04:f0:21:14:c6:5e": // Galileo 40 (GALEXP)
				ch = CHEXP;
				break;
			
			default: // unknown
				break;
		}
		
		return ch;
	}
	
	public void createEmptyGrid()
	{	
		int radius = 5;
		int maxradius = 30;
		int Distance = 70;
		Color InitColor = Color.black;
		
		GAL01 =	new Ball(2*Distance+((maxradius-radius)/2), 7*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL02 =	new Ball(3*Distance+((maxradius-radius)/2), 7*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL03 =	new Ball(4*Distance+((maxradius-radius)/2), 7*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL04 =	new Ball(5*Distance+((maxradius-radius)/2), 7*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL05 =	new Ball(6*Distance+((maxradius-radius)/2), 7*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL06 =	new Ball(7*Distance+((maxradius-radius)/2), 7*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL07 =	new Ball(2*Distance+((maxradius-radius)/2), 6*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL08 =	new Ball(3*Distance+((maxradius-radius)/2), 6*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL09 =	new Ball(4*Distance+((maxradius-radius)/2), 6*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL10 =	new Ball(5*Distance+((maxradius-radius)/2), 6*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL11 =	new Ball(6*Distance+((maxradius-radius)/2), 6*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL12 =	new Ball(7*Distance+((maxradius-radius)/2), 6*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL13 =	new Ball(2*Distance+((maxradius-radius)/2), 5*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL14 =	new Ball(3*Distance+((maxradius-radius)/2), 5*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL15 =	new Ball(4*Distance+((maxradius-radius)/2), 5*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL16 =	new Ball(5*Distance+((maxradius-radius)/2), 5*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL17 =	new Ball(6*Distance+((maxradius-radius)/2), 5*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL18 =	new Ball(7*Distance+((maxradius-radius)/2), 5*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL19 =	new Ball(2*Distance+((maxradius-radius)/2), 4*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL20 =	new Ball(3*Distance+((maxradius-radius)/2), 4*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL21 =	new Ball(4*Distance+((maxradius-radius)/2), 4*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL22 =	new Ball(5*Distance+((maxradius-radius)/2), 4*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL23 =	new Ball(6*Distance+((maxradius-radius)/2), 4*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL24 =	new Ball(7*Distance+((maxradius-radius)/2), 4*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL25 =	new Ball(2*Distance+((maxradius-radius)/2), 3*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL26 =	new Ball(3*Distance+((maxradius-radius)/2), 3*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL27 =	new Ball(4*Distance+((maxradius-radius)/2), 3*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL28 =	new Ball(5*Distance+((maxradius-radius)/2), 3*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL29 =	new Ball(6*Distance+((maxradius-radius)/2), 3*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL30 =	new Ball(7*Distance+((maxradius-radius)/2), 3*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL31 =	new Ball(2*Distance+((maxradius-radius)/2), 2*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL32 =	new Ball(3*Distance+((maxradius-radius)/2), 2*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL33 =	new Ball(4*Distance+((maxradius-radius)/2), 2*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL34 =	new Ball(5*Distance+((maxradius-radius)/2), 2*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL35 =	new Ball(6*Distance+((maxradius-radius)/2), 2*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GAL36 =	new Ball(7*Distance+((maxradius-radius)/2), 2*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
		GALEXP = new Ball(1*Distance+((maxradius-radius)/2), 8*Distance+((maxradius-radius)/2), radius, 0, 0, InitColor);
	}
	
	public void createEmptyCH()
	{
		int length = 32;
		int maxlength = 30;
		int Distance = 70;
		Color InitColor = Color.white;
		
		CH01 =	new CH(2*Distance+((maxlength-length)/2), 7*Distance+((maxlength-length)/2), length, InitColor);
		CH02 =	new CH(3*Distance+((maxlength-length)/2), 7*Distance+((maxlength-length)/2), length, InitColor);
		CH03 =	new CH(4*Distance+((maxlength-length)/2), 7*Distance+((maxlength-length)/2), length, InitColor);
		CH04 =	new CH(5*Distance+((maxlength-length)/2), 7*Distance+((maxlength-length)/2), length, InitColor);
		CH05 =	new CH(6*Distance+((maxlength-length)/2), 7*Distance+((maxlength-length)/2), length, InitColor);
		CH06 =	new CH(7*Distance+((maxlength-length)/2), 7*Distance+((maxlength-length)/2), length, InitColor);
		CH07 =	new CH(2*Distance+((maxlength-length)/2), 6*Distance+((maxlength-length)/2), length, InitColor);
		CH08 =	new CH(3*Distance+((maxlength-length)/2), 6*Distance+((maxlength-length)/2), length, InitColor);
		CH09 =	new CH(4*Distance+((maxlength-length)/2), 6*Distance+((maxlength-length)/2), length, InitColor);
		CH10 =	new CH(5*Distance+((maxlength-length)/2), 6*Distance+((maxlength-length)/2), length, InitColor);
		CH11 =	new CH(6*Distance+((maxlength-length)/2), 6*Distance+((maxlength-length)/2), length, InitColor);
		CH12 =	new CH(7*Distance+((maxlength-length)/2), 6*Distance+((maxlength-length)/2), length, InitColor);
		CH13 =	new CH(2*Distance+((maxlength-length)/2), 5*Distance+((maxlength-length)/2), length, InitColor);
		CH14 =	new CH(3*Distance+((maxlength-length)/2), 5*Distance+((maxlength-length)/2), length, InitColor);
		CH15 =	new CH(4*Distance+((maxlength-length)/2), 5*Distance+((maxlength-length)/2), length, InitColor);
		CH16 =	new CH(5*Distance+((maxlength-length)/2), 5*Distance+((maxlength-length)/2), length, InitColor);
		CH17 =	new CH(6*Distance+((maxlength-length)/2), 5*Distance+((maxlength-length)/2), length, InitColor);
		CH18 =	new CH(7*Distance+((maxlength-length)/2), 5*Distance+((maxlength-length)/2), length, InitColor);
		CH19 =	new CH(2*Distance+((maxlength-length)/2), 4*Distance+((maxlength-length)/2), length, InitColor);
		CH20 =	new CH(3*Distance+((maxlength-length)/2), 4*Distance+((maxlength-length)/2), length, InitColor);
		CH21 =	new CH(4*Distance+((maxlength-length)/2), 4*Distance+((maxlength-length)/2), length, InitColor);
		CH22 =	new CH(5*Distance+((maxlength-length)/2), 4*Distance+((maxlength-length)/2), length, InitColor);
		CH23 =	new CH(6*Distance+((maxlength-length)/2), 4*Distance+((maxlength-length)/2), length, InitColor);
		CH24 =	new CH(7*Distance+((maxlength-length)/2), 4*Distance+((maxlength-length)/2), length, InitColor);
		CH25 =	new CH(2*Distance+((maxlength-length)/2), 3*Distance+((maxlength-length)/2), length, InitColor);
		CH26 =	new CH(3*Distance+((maxlength-length)/2), 3*Distance+((maxlength-length)/2), length, InitColor);
		CH27 =	new CH(4*Distance+((maxlength-length)/2), 3*Distance+((maxlength-length)/2), length, InitColor);
		CH28 =	new CH(5*Distance+((maxlength-length)/2), 3*Distance+((maxlength-length)/2), length, InitColor);
		CH29 =	new CH(6*Distance+((maxlength-length)/2), 3*Distance+((maxlength-length)/2), length, InitColor);
		CH30 =	new CH(7*Distance+((maxlength-length)/2), 3*Distance+((maxlength-length)/2), length, InitColor);
		CH31 =	new CH(2*Distance+((maxlength-length)/2), 2*Distance+((maxlength-length)/2), length, InitColor);
		CH32 =	new CH(3*Distance+((maxlength-length)/2), 2*Distance+((maxlength-length)/2), length, InitColor);
		CH33 =	new CH(4*Distance+((maxlength-length)/2), 2*Distance+((maxlength-length)/2), length, InitColor);
		CH34 =	new CH(5*Distance+((maxlength-length)/2), 2*Distance+((maxlength-length)/2), length, InitColor);
		CH35 =	new CH(6*Distance+((maxlength-length)/2), 2*Distance+((maxlength-length)/2), length, InitColor);
		CH36 =	new CH(7*Distance+((maxlength-length)/2), 2*Distance+((maxlength-length)/2), length, InitColor);
		CHEXP = new CH(1*Distance+((maxlength-length)/2), 8*Distance+((maxlength-length)/2), length, InitColor);
	}	
	
	private void checkCIMessageTimeStamps()
	{		
		if( !incomingCIMessageTimeStamps.isEmpty() )
		{
			for( int i = 0; i < incomingCIMessageTimeStamps.size(); i++ )
			{
				if( (timeStamp - incomingCIMessageTimeStamps.get(i)) > (3*GlobalDefines.CI_BROADCAST_PERIOD/1000) )
				{
					System.out.println(Util.getCurrentTime() +  ": Lost CH Connection to " + Util.MACToName(globalClusterInfo.get(i).get(0)));
					
					if( roamingFlag )
					{
						statusString = "Status: Lost Connection to CH: " + Util.MACToName(globalClusterInfo.get(i).get(0));
					}
					
					resetDeletedNodes.addAll(globalClusterInfo.get(i));
					
					globalClusterInfo.remove(i);
					allClusterChannels.remove(i);
					incomingCIMessageTimeStamps.remove(i);
					
					RepaintFlag = true;
				}
			}
		}
	}
	
	private void handleReceivedCommand( String senderIP, String data )
	{
		if (!data.trim().contains(GlobalDefines.MESSAGE_SEPARATOR))
			return;

		ArrayList<String> values = new ArrayList<String>(
				Arrays.asList(data.trim().split("\\" + GlobalDefines.MESSAGE_SEPARATOR)));
		
		// TODO: values.get(0) always seems to be empty after splitting
		values.remove(0);
		
//		for (int i=0; i<values.size(); i++)
//			System.out.println(Util.getCurrentTime() +  ": values[" + i + "]: " + values.get(i));
		
		// TODO: get and store command, remove it from values 
		String command = new String(values.get(0));
		values.remove(0);
		
//		System.out.println(Util.getCurrentTime() +  ": Command: " + command);
//		System.out.println(Util.getCurrentTime() +  ": Received Message: " + data);
		
		switch (command)
		{
			case GlobalDefines.CLUSTER_INFO_MESSAGE:
			{
				if( !roamingFlag )
				{
					statusString = "Status: Incoming CI Message from CH: " + Util.MACToName(values.get(0));
				}
				
				int ClusterInfoOverwriteRowIndex = -1;
				
				ArrayList<String> ClusterInfoTmp = new ArrayList<String>(extractCIMessageInformation( data ));
				ArrayList<String> compareCurrentClusterWithTmp = new ArrayList<String>();

				for( int i = 0; i < globalClusterInfo.size(); i++)
				{
					ArrayList<String> globalClusterInfoRow = new ArrayList<String>(globalClusterInfo.get(i));
					
					if( globalClusterInfoRow.contains(ClusterInfoTmp.get(0) ) ) 	// ClusterInfoTmp.get(0) is always CH MAC
					{
						ClusterInfoOverwriteRowIndex = i;
					}
				}
			
				if( ClusterInfoOverwriteRowIndex > -1 )
				{
					// TODO: set CMs to initial status if they do not exist anymore
					if( globalClusterInfo.get(ClusterInfoOverwriteRowIndex).size() > resetDeletedNodes.size() )
					{
						compareCurrentClusterWithTmp = globalClusterInfo.get(ClusterInfoOverwriteRowIndex);
								
						//for (int i=0; i<compareCurrentClusterWithTmp.size(); i++)
						//	System.out.println(Util.getCurrentTime() +  "Nodes in ClusterInfo: " + compareCurrentClusterWithTmp.get(i));
						
						//compareCurrentClusterWithTmp.remove(0);
						for( int i = 0; i < ClusterInfoTmp.size(); i++ )
						{
							if( !compareCurrentClusterWithTmp.contains(ClusterInfoTmp.get(i)) && i!=1 )
							{
								resetDeletedNodes.add(ClusterInfoTmp.get(i));
							}
						}
						
						for (int i=0; i<resetDeletedNodes.size(); i++)
							System.out.println(Util.getCurrentTime() +  "Nodes to delete: " + resetDeletedNodes.get(i));
						
						if( !resetDeletedNodes.isEmpty() )
						{
							RepaintFlag = true;
						}
					}
					
					globalClusterInfo.remove(ClusterInfoOverwriteRowIndex);
					allClusterChannels.remove(ClusterInfoOverwriteRowIndex);
					incomingCIMessageTimeStamps.remove(ClusterInfoOverwriteRowIndex);
					
					globalClusterInfo.add(ClusterInfoTmp);
					allClusterChannels.add(values.get(1));
					incomingCIMessageTimeStamps.add(Util.getTimeStampInSeconds());
					
//					createEmptyGrid();
//					createEmptyCH();
//					globalClusterInfo.set(ClusterInfoOverwriteRowIndex, ClusterInfoTmp);
//					allClusterChannels.set(ClusterInfoOverwriteRowIndex, values.get(1));
//					incomingCIMessageTimeStamps.set(ClusterInfoOverwriteRowIndex, Util.getTimeStampInSeconds());
				}
				else
				{
					RepaintFlag = true;
					globalClusterInfo.add(ClusterInfoTmp);
					allClusterChannels.add(values.get(1));
					incomingCIMessageTimeStamps.add(Util.getTimeStampInSeconds());
				}
				
//				for( int i = 0; i < globalClusterInfo.size(); i++ )
//				{
//					for( int j = 0; j < ClusterInfoTmp.size(); j++ )
//					{
//						if( !globalClusterInfo.contains(ClusterInfoTmp) )
//						{
//							
//						}
//					}
//				}
				
			
//			repaint();
			
			}break;
			
			case GlobalDefines.CLUSTERING_DONE_MESSAGE:
			{
				if( clusteringDoneList.isEmpty() )
				{
					clusteringDoneList.add(senderIP);
				} else {
					if( !clusteringDoneList.contains(senderIP) )
					{
						clusteringDoneList.add(senderIP);
					}
				}
			}break;
			
			case GlobalDefines.GAL40_NEXT_HOP_TO_CH_MESSAGE:
			{
				if( !values.isEmpty() )
				{
					lastGal40NH2CH = values.get(0);
					setGAL40Position(values.get(0), 30, 30, 70);
					GALEXP.radius = 30;
					statusString = "Status: Incoming CI Message from: GAL40";
					startTimeString = "Last update: " + Util.getCurrentTime();
					repaint();
				}
			}break;
		}
	}
	
	private ArrayList<String> extractCIMessageInformation(String data)
	{
		ArrayList<String> ClusterInfo = new ArrayList<String>(
				Arrays.asList(data.trim().split("\\" + GlobalDefines.MESSAGE_SEPARATOR)));
		
		System.out.println("Message data: " + data);
		
		// TODO: ClusterInfo.get(0) always seems to be empty after splitting
		ClusterInfo.remove(0);
		
		// remove command 'CI'
		ClusterInfo.remove(0);
		
//		for (int i=0; i<ClusterInfo.size(); i++)
//			System.out.println(Util.getCurrentTime() +  ": ClusterInfo[" + i + "]: " + ClusterInfo.get(i));
		
		return ClusterInfo;
	}
	
//	private String myEthIfName;
//	private String myEthernetIP;
	
	private Runnable receiverTCP;
	private ServerSocket receiveTCPSocket;
	
	private int timeStamp;
//	private int startTimeInt;
	private boolean RepaintFlag;
//	private int currentTimeInt;
	private String startTimeString;
//	private String elapsedTimeString;
	private String statusString;
	
	private boolean roamingFlag;
	
	private ArrayList<ArrayList<String>> globalClusterInfo;		// filled on reception of CI_MSG with all information about CHs and CMs (MAC addresses)
	private List<String> allClusterChannels;					// channel collection based on incoming CI_MSG
	private List<String> resetDeletedNodes;
	private List<Integer> incomingCIMessageTimeStamps;
	private List<String> clusteringDoneList;
	
	// GALEXP
	private int[] gal40position;
	private String lastGal40NH2CH;
	
}
