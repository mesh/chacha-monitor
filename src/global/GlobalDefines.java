package global;

import java.io.File;

public interface GlobalDefines
{
	public final static String ETHERNET_IP = "127.0.0.1";
//	public final static String ETHERNET_IP = "139.30.201.114";
	
	public final static int TCP_ETHERNET_RECEIVE_PORT = 39123;
	
	public final static String MESSAGE_SEPARATOR = "|";

	public final static int CI_BROADCAST_PERIOD = 500; // in milliseconds, time between CI messages
	
	public final static String CLUSTER_INFO_MESSAGE = "CI";
	public final static String CLUSTERING_DONE_MESSAGE = "CDONE";
	public final static String GAL40_NEXT_HOP_TO_CH_MESSAGE = "GAL40NH2CH";
	
	public final static String EXP_NODE = "04:f0:21:14:c6:5e";
	
	// Define tool and system parameters
	public final static File toolPath = new File("/sbin");
}